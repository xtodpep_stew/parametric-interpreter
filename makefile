output.exe: main.o parametric_shape.o primitive_generator.o
	clang++ -std=c++17 -O3 main.o parametric_shape.o primitive_generator.o -std=c++17 -o output.exe

main.o: main.cpp
	clang++ -c -std=c++17 -O3 main.cpp -I./parametric/ -o main.o

primitive_generator.o: ./parametric/* ./parametric/utils/*
	clang++ -c -std=c++17 -O3 parametric/primitive_generator.cpp -I./parametric/ -o primitive_generator.o

parametric_shape.o: ./parametric/parametric_shape.cpp ./parametric/utils/*
	clang++ -c -std=c++17 -O3 parametric/parametric_shape.cpp -I./parametric/ -o parametric_shape.o

