### What is this repository for? ###

This is a tool to generate point data for any shape defined 
by parametric equations. The tool can be used to generate
data for a shape of arbitrary dimension.
The class parametric_interpreter accepts a set of parametric
equations and a set of constraints.
The result is a 2D vector which holds the point data.

The parametric equations must be defined with the signature:
std::vector<double> -> double,
and are passed to the parametric_interpreter object within a
std::vector<std::function<double(std::vector<double>)>>

The constraints are defined as a
std::vector<std::tuple<std::tuple<double, double>, unsigned>>
where std::tuple<double, double> is the minimum and maximum values
and unsigned is the desired resolution.

The number of elements in the parametric equation list must equal 
one more than the number of constraints.

### How do I get set up? ###

Use C++17 to compile
The source has been tested to compile on both clang 5.0+ or gcc 6.0+
The makefile provided automatically compiles the source using clang

To view some sample usage, go to examples/sample-usage.cpp

