//sample-usage.cpp
#include <vector>
#include <functional>
#include <algorithm>
#include <cmath>
#include <iostream>
#include "parametric/parametric_shape.h"

#ifndef M_PI
    const double M_PI = 3.14159265358979323846264338327950288;
#endif

const double M_TAU = 2*M_PI;

//{x1} -> {theta}
auto ellipse_x = [](std::vector<double> args) { return cos(args[0]); };
auto ellipse_y = [](std::vector<double> args) { return sin(args[0]); };
parametric_shape ellipse{
    {ellipse_x, ellipse_y},                     //basis: the parametric equations for the circle
    { {{0.0, M_TAU}, 100} }                     //domain: theta range: [0, 2*pi], in steps of 100
};

//{x1, x2} -> {r, theta}
auto disk_x = [](std::vector<double> args) { return args[0] * cos(args[1]); };
auto disk_y = [](std::vector<double> args) { return args[0] * sin(args[1]); };
auto disk_z = [](std::vector<double> args) { return 0.0; }; //circle is at height 0

parametric_shape disk{
    {disk_x, disk_y, disk_z},                   //basis: the parametric equations for the disk
    { {{0.0, 1.0}, 100}, {{0.0, M_TAU}, 100} }  //domain: theta range: [0, 2*pi], r range: [0, 1], in steps of 100 & 100 respectively
};

//{x1, x2} -> {theta, phi}
auto ellipsoid_x = [](std::vector<double> args) { return cos(args[0]) * sin(args[1]); };
auto ellipsoid_y = [](std::vector<double> args) { return cos(args[0]) * sin(args[1]); };
auto ellipsoid_z = [](std::vector<double> args) { return cos(args[1]); };
parametric_shape ellipsoid{
    {ellipsoid_x, ellipsoid_y, ellipsoid_z},        //basis: the parametric equations for the ellipsoid
    { {{0.0, M_TAU}, 100}, {{0.0, M_PI}, 100} }     //domain: theta range: [0, 2*pi], phi range: [0, pi], in steps of 100 & 100 respectively
};

//{x1, x2} -> {theta, phi}
auto torus_x = [](std::vector<double> args) { return (2 + cos(args[1])) * cos(args[0]); };
auto torus_y = [](std::vector<double> args) { return (2 + cos(args[1])) * sin(args[0]); };
auto torus_z = [](std::vector<double> args) { return sin(args[1]); };
parametric_shape torus{
    {torus_x, torus_y, torus_z},                    //basis: the parametric equations for the torus
    { {{0.0, M_TAU}, 100}, {{0.0, M_TAU}, 100} }    //domain: theta range: [0, 2*pi], phi range: [0, pi], in steps of 100 & 100 respectively
};

//{x1, x2, x3} -> {rho, theta, phi}
auto solid_ellipsoid_x = [](std::vector<double> args) { return args[0] * cos(args[1]) * sin(args[2]); };
auto solid_ellipsoid_y = [](std::vector<double> args) { return args[0] * cos(args[1]) * sin(args[2]); };
auto solid_ellipsoid_z = [](std::vector<double> args) { return args[0] * cos(args[2]); };
auto solid_ellipsoid_w = [](std::vector<double> args) { return 0.0; }; //hyper_elipsoid is at "height" 0
parametric_shape solid_sphere{
    {solid_ellipsoid_x, solid_ellipsoid_y, solid_ellipsoid_z, solid_ellipsoid_w},   //basis: the parametric equations for the hyper_ellipsoid
    { {{0.0, 1.0}, 100}, {{0.0, M_TAU}, 100}, {{0.0, M_PI}, 100} }                  //domain: rho range: [0, 1], theta range: [0, 2*pi], phi range: [0, pi], in steps of 100, 100 & 100 respectively
};

//{x0, x1, x2} -> { psi, theta, phi}
auto hypersphere_x = [=](std::vector<double> p){ return cos(p[0]); };
auto hypersphere_y = [=](std::vector<double> p){ return sin(p[0])*cos(p[1]); };
auto hypersphere_z = [=](std::vector<double> p){ return sin(p[0])*sin(p[1])*cos(p[2]); };
auto hypersphere_w = [=](std::vector<double> p){ return sin(p[0])*sin(p[1])*sin(p[2]); };
parametric_shape hyper_ellipsoid{
    {hypersphere_x, hypersphere_y, hypersphere_z, hypersphere_w},
    { {{0.0, M_TAU}, 100}, {{0.0, M_PI}, 100}, {{0.0, M_PI}, 100} } 
};
//.etc