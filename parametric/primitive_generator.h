//primitives.h
#pragma once
#include <list>
#include "parametric_shape.h"
#include "utils/basis-functionals.h"

class primitive_generator
{
    private:
        // use list because memory location of elements is static for 
        // the list's lifetime 
        std::list<parametric_shape> shapes; 
    public:
        parametric_shape* ellipse(double, double, unsigned);
        parametric_shape* torus(double, double, unsigned, unsigned);
        parametric_shape* diskoid(double, double, double, unsigned, unsigned);
        parametric_shape* ellipsoid(double, double, double, unsigned, unsigned);
        std::list<parametric_shape>* get_list();
};

