#include "parametric_shape.h"

parametric_shape::parametric_shape(const basis_list& basis, const domain_list& domain)
{
    this->dimension = basis.size() - 1;
    this->basis = basis;
    this->domain = domain;
    if (validate())
    {
        puts("Initialization: Success\n");
        perform();
    }
    else 
        puts("Initialization: Failure\n");


}

void parametric_shape::traverse(const std::vector<std::vector<double>>& segmentations)
{
    if(segmentations.size() == 1)
        for(const auto& o: segmentations.back())
        {
            point.emplace_back(o);
            result.emplace_back();
            assert(point.size() == dimension);
            for(const auto& f : basis)
                result.back().emplace_back(f(point));
            point.pop_back();
        }
    else if(segmentations.size() > 1)
        for(const auto& o : segmentations.back())
        {
            point.emplace_back(o);
            auto sub_segmentations = segmentations;
            sub_segmentations.pop_back();
            traverse(sub_segmentations);
            point.pop_back();
        }
    else
        std::cerr << "Error: Dimension is invalid\n";
}

void parametric_shape::perform()
{
    auto segmentations = segment_each(domain); // come back to this
    assert(segmentations.size() == dimension);
    traverse(segmentations);
}

std::vector<std::vector<double>> parametric_shape::get_result()
{
    return result;
}

bool parametric_shape::validate()
{
    bool basis_size = false, domain_size = false;

    if(basis.size() == (dimension + 1))
    {
        basis_size = true;
        puts("Basis Size: Success\n");
    }
    else
    { 
        puts("Basis Size: failure\n");
    }
    
    if (domain.size() == dimension)
    {
        domain_size = true;
        puts("Domain Size: Success\n");
    }
    else 
    {
        puts("Domain Size: Failure\n");
    }
    
    return (basis_size && domain_size);
}
