#include "primitive_generator.h"

parametric_shape* primitive_generator::ellipse(double r1, double r2, unsigned steps_theta)
{
    auto basis = ellipse_basis(r1, r2);
    std::vector<std::tuple<std::tuple<double, double>, unsigned>> domain = 
        { {{0.0, M_TAU}, steps_theta} };
    shapes.emplace_back(basis, domain);
    return &shapes.back();
}

parametric_shape* primitive_generator::torus(double r1, double r2, unsigned steps_theta, unsigned steps_phi)
{
    auto basis = torus_basis(r1, r2);
    std::vector<std::tuple<std::tuple<double, double>, unsigned>> domain = 
        { {{0.0, M_TAU}, steps_theta}, {{0.0, M_TAU}, steps_phi} };
    shapes.emplace_back(basis, domain);
    return &shapes.back();
}

parametric_shape* primitive_generator::diskoid(double r1, double r2, double h, unsigned steps_r, unsigned steps_theta)
{
    auto basis = diskoid_basis(r1, r2, h);
    std::vector<std::tuple<std::tuple<double, double>, unsigned>> domain = 
        { {{0.0, M_TAU}, steps_theta}, {{0.0, 1.0}, steps_r} };
    shapes.emplace_back(basis, domain);
    return &shapes.back();

}

parametric_shape* primitive_generator::ellipsoid(double r1, double r2, double r3, unsigned steps_theta, unsigned steps_phi)
{
    auto basis = ellipsoid_basis(r1, r2, r3);
    std::vector<std::tuple<std::tuple<double, double>, unsigned>> domain = 
        { {{0.0, M_PI}, steps_theta},{{0.0, M_TAU}, steps_phi} };
    shapes.emplace_back(basis, domain);
    return &shapes.back();
}

std::list<parametric_shape>* primitive_generator::get_list()
{
    return &shapes;
}