//parametric_shape.h
#pragma once
#include <vector>
#include <tuple>
#include <algorithm>
#include <functional>
#include <iostream>
#include <string>
#include "utils/xtoph-utils.h"

#ifndef M_PI
    const double M_PI = 3.14159265358979323846264338327950288;
#endif

const double M_TAU = 2*M_PI;

using basis_list = std::vector<std::function<double(std::vector<double>)>>;
using domain_list = std::vector<std::tuple<std::tuple<double, double>, unsigned>>;

class parametric_shape
{
    private:
        basis_list basis;
        domain_list domain;
        std::vector<std::vector<double>> result;
        unsigned dimension;
        std::vector<double> point;
        void perform();
        bool validate();
        void traverse(const std::vector<std::vector<double>>&);
    public:
        parametric_shape(const basis_list&, const domain_list&);
        std::vector<std::vector<double>> get_result();
};