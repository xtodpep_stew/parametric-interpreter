//basis-functionals.h
#pragma once
#include <vector>
#include <cmath>
#include <functional>

using basis_list = std::vector<std::function<double(std::vector<double>)>>;

auto ellipse_basis = 
    [](double r1, double r2) -> basis_list
    {
        std::function<double(std::vector<double>)> ellipse_x = 
            [r1](std::vector<double> x){ return r1*std::cos(x[0]); };
        std::function<double(std::vector<double>)> ellipse_y = 
            [r2](std::vector<double> x){ return r2*std::sin(x[0]); };
        return {ellipse_x, ellipse_y};
    };

auto ellipsoid_basis = 
    [](double r1, double r2, double r3) -> basis_list
    {
        std::function<double(std::vector<double>)> ellipsoid_x = 
            [r1](std::vector<double> x) { return r1 * std::cos(x[0]) * std::sin(x[1]); };
        std::function<double(std::vector<double>)> ellipsoid_y = 
            [r2](std::vector<double> x) { return r2 * std::sin(x[0]) * std::sin(x[1]); };
        std::function<double(std::vector<double>)> ellipsoid_z = 
            [r3](std::vector<double> x) { return r3 * std::cos(x[1]); };
        return {ellipsoid_x, ellipsoid_y, ellipsoid_z};
    };

auto torus_basis = 
    [](double r1, double r2) -> basis_list
    {
        std::function<double(std::vector<double>)> torus_x = 
            [r1, r2](std::vector<double> x) { return (r1 + r2*std::cos(x[1])) * std::cos(x[0]); };
        std::function<double(std::vector<double>)> torus_y = 
            [r1, r2](std::vector<double> x) { return (r1 + r2*std::cos(x[1])) * std::sin(x[0]); };
        std::function<double(std::vector<double>)> torus_z = 
            [r1, r2](std::vector<double> x) { return r2 * std::sin(x[1]); };
        return {torus_x, torus_y, torus_z};
    };

auto diskoid_basis = 
    [](double r1, double r2, double h) -> basis_list
    {
        std::function<double(std::vector<double>)> disk_x = 
            [r1](std::vector<double> x) { return r1 * x[0] * std::cos(x[1]); };
        std::function<double(std::vector<double>)> disk_y = 
            [r2](std::vector<double> x) { return r2 * x[0] * std::sin(x[1]); };
        std::function<double(std::vector<double>)> disk_z = 
            [h](std::vector<double> x) { return h; }; //circle is at "height" 0
        return {disk_x, disk_y, disk_z};
    };
