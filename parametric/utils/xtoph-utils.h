//xtoph-utils.h
#pragma once
#include <vector>
#include <cassert>
#include <string>
#include <iostream>
#include <iomanip>
#include <algorithm>

auto put_vector =
    [](const auto& vect, const auto& buff = std::cout) -> void
    {
        buff << std::setprecision(10);
        buff << std::fixed;
        buff << "{";
        for_each(vect.begin(), vect.end()-1, [&buff](auto val)
        {
            buff << val << ", ";
        });
        buff << vect.back() << "}\n";
    };

auto put_vector_csv =
    [](const auto& vect, auto& buff = std::cout) -> void
    {
        buff << std::setprecision(10);
        buff << std::fixed;
        for_each(vect.begin(), vect.end()-1, [&buff](auto val)
        {
            buff << val << ", ";
        });
        buff << vect.back() << '\n';
    };

auto put_vector_list = 
    [](const auto& vectors, const auto& printer, auto& buff = std::cout) -> void
    {
        for(const auto& vector : vectors)
            printer(vector, buff);
    };


auto linspace =
    [](double a, double b, unsigned n) -> std::vector<double>
    {
        std::vector<double> linspaced;
        double step = (b - a)/(n - 1);

        for(unsigned i = 0; i < n; i++)
        {
            linspaced.push_back(a + step*i);
        }
        
        return linspaced;
    };

auto all_match =
    [](const auto& propositions, const auto& match) -> bool
    {
        auto result = true;
        for(const auto& proposition : propositions)
        {
            if(proposition != match)
            {
                result = false;
                break;
            }
        }
        return result;
    };

auto segment_each =
    [](const auto ranges) -> std::vector<std::vector<double>>
    {
        std::vector<std::vector<double>> segmentations;
        for(const auto& [range, step] : ranges)
        {
            const auto& [min, max] = range;
            segmentations.emplace_back(linspace(min, max, step));
        }
        return segmentations;
    };