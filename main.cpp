#include <vector>
#include <cstdlib>
#include <functional>
#include <algorithm>
#include <cmath>
#include <iostream>
#include <fstream>
#include <memory>
#include "utils/xtoph-utils.h"
#include "parametric_shape.h"
#include "primitive_generator.h"

int main(int argc, char** argv)
{
    if(argc != 5)
    {
        std::cout << "usage: " << argv[0] << " r1 r2 r3 r4\n";
        return 1;
    }

    double r1, r2, r3, r4;
    r1 = std::atof(argv[1]);
    r2 = std::atof(argv[2]);
    r3 = std::atof(argv[3]);
    r4 = std::atof(argv[4]);
    
    std::ofstream output;
    output.open("output.csv");

    //{x0, x1, x2} -> { psi, theta, phi}
    auto x0 = [=](std::vector<double> p){ return r1*cos(p[0]); };
    auto x1 = [=](std::vector<double> p){ return r2*sin(p[0])*cos(p[1]); };
    auto x2 = [=](std::vector<double> p){ return r3*sin(p[0])*sin(p[1])*cos(p[2]); };
    auto x3 = [=](std::vector<double> p){ return r4*sin(p[0])*sin(p[1])*sin(p[2]); };
    parametric_shape hyper_ellipsoid{
        {x0, x1, x2, x3},
        { {{0.0, M_TAU}, 100}, {{0.0, M_PI}, 100}, {{0.0, M_PI}, 100} } 
    };
    auto result = hyper_ellipsoid.get_result();
    put_vector_list(hyper_ellipsoid.get_result(), put_vector_csv, output);
    return 0;
}